package alp4

import . "condition"

var (
	nR, nW uint
	okR, okW Condition
)

func ReaderIn() {
	if nW > 0 {
		okR.Wait()
	}
	nR++
	okR.Signal()
}

func ReaderOut() {
	nR--
	if nR == 0 {
		okW.Signal()
	}
}

func WriterIn() {
	if nR > 0 || nW > 0 {
		okW.Wait()
	}
	nW = 1
}

func WriterOut() {
	nW = 0
	if okR.Awaited() {
		okR.Signal()
	} else {
		okW.Signal()
	}
}


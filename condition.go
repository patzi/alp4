package condition

type Condition interface {

	// blockiert den aufrufenden Prozess und gibt den Monitor wieder frei
	Wait()

	// der erste blockierte Prozess wird wieder deblockiert
	Signal()

	// true wenn min ein Prozess blockiert wird
	Awaited() bool

}

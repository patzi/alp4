package main

import("math/rand"; . "time"; "fmt")
var (Counter int; done chan bool)

func wait() { Sleep(Duration(rand.Int63n(1e5))) }

func inc (n *int) {
	Accu := *n	// "LDA n"
	wait()
	Accu ++		// "INA"
	wait()
	*n = Accu 	// "STA n"
	fmt.Println(*n)
	wait()
}

func count(p int) {
	const N = 5
	for n:=0; n < N; n++ {
		inc (&Counter)
	}
	done <- true
}

func main() {
	Counter = 0
	done = make (chan bool)
	go count(0); go count(1)
	<- done; <- done
	fmt.Printf("Zähler %d\n", Counter)
}


package main

import(
	"sync"
	"fmt"
	"./tree/tree.go"
)

var(
	knoten1, knoten2 [10] int
	mutex1, mutex2 Mutex
	pos1, pos2 int
)

func Compare(t1, t2 *Tree) bool {
	go Walk(t1, knoten1, pos1, mutex1) // Writer1
	go Walk(t2, knoten2, pos2, mutex2) // Writer2
	value1, value2 := 0, 0
	for i := 0; ; i++ {
		mutex1.Lock
		value1 = knoten1[i]
		mutex1.Unlock
		mutex2.Lock
		value2 = knoten2[i]
		mutex2.Unlock
		if value1 != value2b {
			return false
		}
	}
	return true
}

func Walk(t *Tree, a [10] int, pos int, m Mutex) {
	if t == nil {
		return
	}
	Walk(t.Left)
	m.Lock
	a[pos] = t.Value
	pos++
	m.Unlock
	Walk(t.Right)
}

func main() {
	baum1, baum2 := Tree.New(1), Tree.New(2)
}

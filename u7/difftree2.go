package main

import(
	"sync"
	"fmt"
        "math/rand"
)

var(
	knoten1, knoten2 [11] int
	mutex1, mutex2, mutexSleep1, mutexSleep2 sync.Mutex
	pos1, pos2, pipe1, pipe2	 int
)

func Compare(t1, t2 *Tree) bool {
	go Walk1(t1) // Writer1
	go Walk2(t2) // Writer2
	var value1, value2 int
//	value1, value2 := 0, 0
	for i := 0; i < 10; i++ {
		mutex1.Lock()
		pipe1--
		if (pipe1 == -1) {
			mutexSleep1.Lock()
		}
		value1 = knoten1[i]
		mutex1.Unlock()
		mutex2.Lock()
		pipe2--
		if (pipe2 == -1) {
			mutexSleep2.Lock()
		}
		value2 = knoten2[i]
		mutex2.Unlock()
		fmt.Println(value1, value2)
		if value1 != value2 {
			fmt.Println("false")
			return false
		}
	}
	return true
}

func Walk1(t *Tree) {
	if t == nil {
		return
	}
	Walk1(t.Left)
	mutex1.Lock()
	knoten1[pos1] = t.Value
	pos1++
	pipe1++
	if (pipe1 == 0) {
		// was -1, which indicated sleeping reader
		mutexSleep1.Unlock()
	}
	mutex1.Unlock()
	Walk1(t.Right)
}
func Walk2(t *Tree) {
	if t == nil {
		return
	}
	Walk2(t.Left)
	mutex2.Lock()
	knoten2[pos2] = t.Value
	pos2++
	pipe2++
	if (pipe2 == 0) {
		// was -1, which indicated sleeping reader
		mutexSleep2.Unlock()
	}
	mutex2.Unlock()
	Walk2(t.Right)
}

func main() {
	baum1, baum2 := New(1), New(2)
	fmt.Println(baum1.String())
	fmt.Println(baum2.String())
	bla := Compare(baum1, baum2)
	fmt.Println(bla)
}

// A Tree is a binary tree with integer values.
type Tree struct {
        Left  *Tree
        Value int
        Right *Tree
}

// New returns a new, random binary tree holding the values k, 2k, ..., 10k.
func New(k int) *Tree {
        var t *Tree
        for _, v := range rand.Perm(10) {
                t = insert(t, (1+v)*k)
        }
        return t
}

func insert(t *Tree, v int) *Tree {
        if t == nil {
                return &Tree{nil, v, nil}
        }
        if v < t.Value {
                t.Left = insert(t.Left, v)
        } else {
                t.Right = insert(t.Right, v)
        }
        return t
}

func (t *Tree) String() string {
        if t == nil {
                return "()"
        }
        s := ""
        if t.Left != nil {
                s += t.Left.String() + " "
        }
        s += fmt.Sprint(t.Value)
        if t.Right != nil {
                s += " " + t.Right.String()
        }
        return "(" + s + ")"
}


ALP4
======

Das ist das Repo zum bearbeiten der Übungszettel für [ALP4](http://www.inf.fu-berlin.de/lehre/SS13/alp4/)

Nützliche Links:
-----

* [sync package](http://golang.org/pkg/sync/) von Go 
* [The Go Programming Language Specification](http://golang.org/ref/spec)
* [The Little Book of Semaphores](http://greenteapress.com/semaphores/)